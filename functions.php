<?php
function mychildtheme_enqueue_styles() {
   // does 'parent-style' need to be 'understrap'?
   wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
   // enqueue parent/css directory as well? I hope this works
   wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/css/' );
   wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css',
      array('parent-style');
}
add_action( 'wp_enqueue_scripts', 'mychildtheme_enqueue_styles' );
?>
